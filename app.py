import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import io
import requests
import sys
import os

app = dash.Dash()

user = os.environ['USER']
pwd = os.environ['PWD']
# urlrpcgif = 'https://rpcgif:RPCgem2018@dfs.cern.ch/dfs/users/r/rpcgif/public/rpc-monitoring.csv'
urlrpclab = f'https://{user}:{pwd}@dfs.cern.ch/dfs/users/r/rpcgif/public/RPC analysis/lab256/newrpc/currents.txt'

# rpcgifdata = requests.get(urlrpcgif).content
rpclabdata = requests.get(urlrpclab).content


# dfgif = pd.read_csv(io.StringIO(rpcgifdata.decode('utf-8')), header=None, names=gifnames)
df = pd.read_csv(io.StringIO(rpclabdata.decode('utf-8')), header=None,
                 names=['channel', 'hv', 'i', 'status', 'timestamp'], parse_dates=['timestamp'])

app.layout = html.Div([
    html.H1(children='RPC256'),
    html.H2(children=f'Last update on {df.timestamp.iat[-1]}'),
    html.Table(
        [html.Tr([
            html.Th('RPC'),
            html.Th('HV [V]'),
            html.Th('Current [uA]')
        ])] +
        [html.Tr([
            html.Td(ch + 2),
            html.Td(chgroup['hv'].iat[-1]),
            html.Td(f"{chgroup['i'].iat[-1]:.2f}"),

        ]) for ch, chgroup in df.groupby('channel') if ch != 4]
    ),
    dcc.Graph(
        id='current',
        figure={
            'data': [
                go.Scatter(
                    x=dfch.timestamp,
                    y=dfch[dfch.channel == ch]['i'],
                    opacity=0.7,
                    marker={
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=f'RPC {ch + 2}'
                ) for ch, dfch in df.groupby('channel') if ch != 4
            ],
            'layout': go.Layout(
                xaxis={'title': 'Timestamp'},
                yaxis={'title': 'Current [uA]', 'range': [0, 1.5]},
                legend={'x': 0, 'y': 1},
                hovermode='closest'
            )
        }
    ),
    dcc.Graph(
        id='hv',
        figure={
            'data': [
                go.Scatter(
                    x=dfch.timestamp,
                    y=dfch[dfch.channel == ch]['hv'],
                    opacity=0.7,
                    marker={
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=f'RPC {ch + 2}'
                ) for ch, dfch in df.groupby('channel') if ch != 4
            ],
            'layout': go.Layout(
                xaxis={'title': 'Timestamp'},
                yaxis={'title': 'HV [uA]', 'range': [0, 10000]},
                legend={'x': 0, 'y': 1},
                hovermode='closest'
            )
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')
