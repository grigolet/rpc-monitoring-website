FROM python:3
EXPOSE 8050
RUN pip install --upgrade pip
RUN pip install dash==0.21.1
RUN pip install dash-renderer==0.13.0
RUN pip install dash-html-components==0.11.0
RUN pip install dash-core-components==0.23.0
RUN pip install plotly --upgrade
RUN pip install pandas==0.23.0
RUN pip install request
WORKDIR /app
CMD ["python", "app.py"]